﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransfer
{
    class Client
    {
               public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }
        static void Main(string[] args)
        {
            try
            {
                string startFilePath = @"C:\Users\faids\Downloads\New folder\untuk kirim file\gambar.jpg";

                //Membuat password dalam bentuk string
                Random ase = new Random();
                int pass = ase.Next();
                string kuncirahasia = pass.ToString();


                EncryptFile(startFilePath, kuncirahasia);
                Console.WriteLine("file berhasil dienkrip dengan password : " + kuncirahasia);

                string startZipPath = @"C:\Users\faids\Downloads\New folder\enkrip";
                string zipPath = @"C:\Users\faids\Downloads\New folder\kompres\gambar.zip";

                ZipFile.CreateFromDirectory(startZipPath, zipPath);
                Console.WriteLine("file berhasil dikompres\n");

                //Mencoba menyambungkan ke server
                Console.WriteLine("Trying to connect to the server...\n");
                TcpClient tcpClient = new TcpClient("127.0.0.1", 8080);
                Console.WriteLine("Connected. File Sended.");

                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());

                sWriter.WriteLine("gambar.zip");
                sWriter.WriteLine(kuncirahasia); 

                byte[] bytes = File.ReadAllBytes(zipPath);
                sWriter.WriteLine(bytes.Length.ToString());
                sWriter.Flush();

                sWriter.WriteLine(zipPath);
                sWriter.Flush();

                tcpClient.Client.SendFile(zipPath);

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }

        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
        }
    }
}
