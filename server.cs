﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace FileTf
{
    class Server
    {
        static void Main(string[] args)
        {
            try
            {
                
                TcpListener tcpListener = new TcpListener(IPAddress.Any, 8080);
                tcpListener.Start();

                Console.WriteLine("Server started");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    Console.WriteLine("Connected to client");

                    StreamReader reader = new StreamReader(tcpClient.GetStream());

                    
                    string pass = reader.ReadLine();
                    string namafile = reader.ReadLine();
                    string cmdukuranfile = reader.ReadLine();
                    string cmdnamafile = reader.ReadLine();

                    //Menerima file dari client
                    int length = Convert.ToInt32(cmdukuranfile);
                    byte[] buffer = new byte[length];
                    int received = 0;
                    int read = 0;
                    int size = 1024;
                    int remaining = 0;

                    while (received < length)
                    {
                        remaining = length - received;
                        if (remaining < size)
                        {
                            size = remaining;
                        }

                        read = tcpClient.GetStream().Read(buffer, received, size);
                        received += read;
                    }

                    using (FileStream fStream = new FileStream(Path.GetFileName(cmdnamafile), FileMode.Create))
                    {
                        fStream.Write(buffer, 0, buffer.Length);
                        fStream.Flush();
                        fStream.Close();
                    }

                    //Menampilkan tulisan file yang diterima disimpan dalam folder project
                    Console.WriteLine("\nFile tersimpan di " + Environment.CurrentDirectory);

                    int unicode = 92;
                    char character = (char)unicode;
                    string text = character.ToString();

                    //Mengatur letak file zip yang diterima dan letak file akan diekstrak
                    string zipPath = Environment.CurrentDirectory + text + namafile;
                    string extractPath = @"C:\Users\faids\Downloads\New folder\ekstrak";

                    string startFilePath = @"C:\Users\faids\Downloads\New folder\ekstrak\gambar.jpg";
                    string endFilePath = @"C:\Users\faids\Downloads\New folder\dekrip\gambar.jpg";

                    ZipFile.ExtractToDirectory(zipPath, extractPath);
                    Console.WriteLine("\nFile terekstrak di " + extractPath);

                    DecryptFile(startFilePath, endFilePath, pass);
                    Console.WriteLine("\nFile berhasil didekrip dengan password : " + pass);
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        //Fungsi untuk Dekripsi File
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    //alogaritma aes
                    AES.BlockSize = 128;
                    AES.KeySize = 256;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            // mengubah menjadi byte
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }

    }
}
